﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
/* 
 * @(#)fractal.java - Fractal Program Mandelbrot Set 
 * www.eckhard-roessel.de
 * Copyright (c) Eckhard Roessel. All Rights Reserved.
 * 06/30/2000 - 03/29/2000
 */
//import java.awt.*;
//import java.applet.*;   Not Needed
//import java.awt.event.*;
/** 
 * @version 1.1
 * @author Eckhard Roessel
 * @modified 03/29/2001
 * @modified 08/16/2006 tweaked by Duncan Mullier 8/2006
 */
namespace Assignment_Part1
{
    //public class Fractal extends Applet implements MouseListener, MouseMotionListener
    //{  Not Needed
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            init();
            start();
        }
        public struct HSBColor
        {
            float h;
            float s;
            float b;
            int a;
            public HSBColor(float h, float s, float b)
            {
                this.a = 0xff;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }
            public HSBColor(int a, float h, float s, float b)
            {
                this.a = a;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }
            public float H
            {
                get { return h; }
            }
            public float S
            {
                get { return s; }
            }
            public float B
            {
                get { return b; }
            }
            public int A
            {
                get { return a; }
            }
            public Color Color
            {
                get
                {
                    return FromHSB(this);
                }
            }
            public static Color FromHSB(HSBColor hsbColor)
            {
                float r = hsbColor.b;
                float g = hsbColor.b;
                float b = hsbColor.b;
                if (hsbColor.s != 0)
                {
                    float max = hsbColor.b;
                    float dif = hsbColor.b * hsbColor.s / 255f;
                    float min = hsbColor.b - dif;
                    float h = hsbColor.h * 360f / 255f;
                    if (h < 60f)
                    {
                        r = max;
                        g = h * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = max;
                        b = min;
                    }
                    else if (h < 180f)
                    {
                        r = min;
                        g = max;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = min;
                        g = -(h - 240f) * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h <= 360f)
                    {
                        r = max;
                        g = min;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                    }
                }
                return Color.FromArgb
                    (
                        hsbColor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                        );
            }
        }
        private const int MAX = 256;      // max iterations
        //private final int MAX = 256;    not needed
        private const double SX = -2.025; // start value real
        //private final double SX = -2.025; not needed
        private const double SY = -1.125; // start value imaginary
        //private final double SY = -1.125; not needed
        private const double EX = 0.6;    // end value real
        //private final double EX = 0.6;  not needed
        private const double EY = 1.125;  // end value imaginary
        //private final double EY = 1.125;  not needed
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished;
        //private static boolean action, rectangle, finished; changed to bool
        private static float xy;
        //private Image picture;
        private Graphics g1;
        private HSBColor HSBcol = new HSBColor();
        private Bitmap bitmap;
        //private HSB HSBcol = new HSB(); changed to hsbcolor
        public void init() // all instances will be prepared
        {
            //HSBcol = new HSB();
            this.Size = new Size(640, 480);
            //setSize(640, 480); not needed
            finished = false;
            //addMouseListener(this); not needed
            //addMouseMotionListener(this); not needed
            //c1 = new Cursor(Cursor.WAIT_CURSOR); not needed
            //c2 = new Cursor(Cursor.CROSSHAIR_CURSOR); not needed
            //x1 = getSize().width; not needed
            //y1 = getSize().height; not needed
            //x1 = this.pictureBox1.Width; 
            //y1 = this.pictureBox1.Height;
            x1 = 640;
            y1 = 480;
            xy = (float)x1 / (float)y1;
            //picture = createImage(x1, y1); not needed
            //g1 = picture.getGraphics(); not needed
            //bitmap = new Bitmap(x1, y1, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            bitmap = new Bitmap(x1, y1);
            g1 = Graphics.FromImage(bitmap);

            //Pen myPen = new Pen(System.Drawing.Color.Plum, 3);
            //g1.DrawRectangle(myPen, 0, 0, 0, 0);
            /*
            if (rectangle)
            {
                
                if (xs < xe)
                {
                    if (ys < ye) g1.DrawRectangle(myPen, xs, ys, (xe - xs), (ye - ys));
                    else g1.DrawRectangle(myPen, xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                    if (ys < ye) g1.DrawRectangle(myPen, xe, ys, (xs - xe), (ye - ys));
                    else g1.DrawRectangle(myPen, xe, ye, (xs - xe), (ys - ye));
                }
            }*/

            //Pen myPen = new Pen(System.Drawing.Color.Plum, 3);
            //Rectangle rectangleObj = new Rectangle(10, 10, 200, 200);
            //g1.DrawEllipse(myPen, rectangleObj);
            //g1.Dispose();
            finished = true;
        }
        public void destroy() // delete all instances 
        {
            if (finished)
            {
                //removeMouseListener(this); not needed
                //removeMouseMotionListener(this); not needed
                //picture = null;
                bitmap = null;
                g1 = null;
                //c1 = null; not needed
                //c2 = null; not needed
                //System.gc(); // garbage collection changed
                GC.Collect(); // garbage collection
            }
        }
        public void start()
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();
        }
        public void stop()
        {
        }

        public void paint(Graphics g1)
        {
            update(g1);
        }

        public void update(Graphics g1)
        {/*
		    g.drawImage(picture, 0, 0, this);
		    if (rectangle)                          
		    {
			    g.setColor(Color.white);
			    if (xs < xe)
			    {
				    if (ys < ye) g.drawRect(xs, ys, (xe - xs), (ye - ys));          
				    else g.drawRect(xs, ye, (xe - xs), (ys - ye));
			    }
			    else
			    {
				    if (ys < ye) g.drawRect(xe, ys, (xs - xe), (ye - ys));
				    else g.drawRect(xe, ye, (xs - xe), (ys - ye));
			    }
		
            }*/
        }

        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b, alt = 0.0f;
            action = false;
            //setCursor(c1); not needed
            //showStatus("Mandelbrot-Set will be produced - please wait..."); not needed
            Color color = Color.Black;
            Pen myPen = new Pen(color);
            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y);
                    if (h != alt)
                    {
                        b = 1.0f - h * h; //brightness
                        ///djm added
                        ///HSBcolor.fromHSB(h,0.8f,b); //convert hsb to rgb then make a Java Color
                        ///Color col = new Color(0,HSBcolor.rChan,HSBcolor.gChan,HSBcolor.bChan);
                        ///g1.setColor(col);
                        //djm end
                        //djm added to convert to RGB from HSB
                        //g1.setColor(Color.getHSBColor(h, 0.8f, b));
                        color = HSBColor.FromHSB(new HSBColor(h * 255, 0.8f * 255, b * 255));
                        //djm test
                        //Color col = Color.getHSBColor(h,0.8f,b);
                        //int red = col.getRed();
                        //int green = col.getGreen();
                        //int blue = col.getBlue();
                        //djm
                        myPen = new Pen(color);
                        alt = h;
                    }
                    g1.DrawLine(myPen, x, y, x + 1, y);
                }
            //showStatus("Mandelbrot-Set ready - please select zoom area with pressed mouse.");
            //setCursor(c2);
            action = true;
        }

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;
            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }
        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
        }
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g1 = e.Graphics;
            g1.DrawImage(bitmap, 0, 0, x1, y1);
            g1.Dispose();
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            //e.consume();
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                rectangle = true;
                //repaint();
            }
        }
        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            //e.consume();
            if (action)
            {
                xs = e.X;
                ys = e.Y;
            }
        }
        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            int z, w;
            //e.consume();
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                rectangle = false;
                //repaint();
                Refresh();
            }
        }



    }
}