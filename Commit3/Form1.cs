﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment_part2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            Image bp = new Bitmap(this.pictureBox1.Height, this.pictureBox1.Width);
            Graphics g1 = Graphics.FromImage(bp);
            pictureBox1.Image = bp;
        }

        public struct HSBColor
        {
            float h;
            float s;
            float b;
            int a;
            public HSBColor(float h, float s, float b)
            {
                this.a = 0xff;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }
            public HSBColor(int a, float h, float s, float b)
            {
                this.a = a;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }
            public float H
            {
                get { return h; }
            }
            public float S
            {
                get { return s; }
            }
            public float B
            {
                get { return b; }
            }
            public int A
            {
                get { return a; }
            }
            public Color Color
            {
                get
                {
                    return FromHSB(this);
                }
            }

            public static Color FromHSB(HSBColor hsbColor)
            {
                float r = hsbColor.b;
                float g = hsbColor.b;
                float b = hsbColor.b;
                if (hsbColor.s != 0)
                {
                    float max = hsbColor.b;
                    float dif = hsbColor.b * hsbColor.s / 255f;
                    float min = hsbColor.b - dif;
                    float h = hsbColor.h * 360f / 255f;
                    if (h < 60f)
                    {
                        r = max;
                        g = h * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = max;
                        b = min;
                    }
                    else if (h < 180f)
                    {
                        r = min;
                        g = max;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = min;
                        g = -(h - 240f) * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h <= 360f)
                    {
                        r = max;
                        g = min;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                    }
                }
                return Color.FromArgb
                    (
                        hsbColor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0f), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                        );
            }

        }
        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool action, rectangle, finished;
        private static float xy;
        private Image bp;
        private Image picture;


        private void button1_Click(object sender, EventArgs e)
        {
            // ddanbe Start button click code
            init();
            start();
            // Refresh(); //----->added ddanbe
        }

        private Graphics g1;
        //private HSB HSBcol=new HSB();
        public void init() // all instances will be prepared    
        {
            //HSBcol = new HSB();        
            //this.Size = new Size(640, 480);  
            // ---->Size done in properties of pictureBox1
            // also add FixedSingle as BorderStyle
            // ---->Set size of form in designer accordingly
            finished = false;
            x1 = this.pictureBox1.Width;  //---->added    pictureBox1   
            y1 = this.pictureBox1.Height; //---->added    pictureBox1    
            //MessageBox.Show(x1.ToString(), "X1");
            xy = (float)x1 / (float)y1;
            //?JAVA? picture = createImage(x1, y1);  
           bp = new Bitmap(x1, y1);
            //?JAVA? g1 = picture.getGraphics();  
            g1 = this.CreateGraphics();
            finished = true;
            //mandelbrot();   ---> is called in Start method 
            //init();
            //start();
        }
        public void destroy() // delete all instances 
        {
            if (finished)
            {
                bp = null;
                g1 = null;
                GC.Collect(); // garbage collection
            }
        }
        public void start()
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();
        }
        public void stop()
        {
        }
        private void mandelbrot() // calculate all points
        {
            //int x, y;
            float h, b, alt = 0.0f;
            action = false;
            statusBox.Text = "Mandelbrot-Set will be produced - please wait...";
            for (int x = 0; x < x1; x += 2)
                for (int y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value
                    if (h != alt)
                    {
                        b = 1.0f - h * h; // brightnes
                       // HSBcolor.fromHSB(h, 0.8f, b);
                        Color color = HSBColor.FromHSB(new HSBColor(h * 255, 0.8f * 255, b * 255));  // VERY IMPORTANT
                                                                                                     //djm 
                        alt = h;
                        Pen pen = new Pen(color);

                     /**   MessageBox.Show(x1.ToString(), "X1");
                        MessageBox.Show(y1.ToString(), "Y1");
                        MessageBox.Show(x.ToString(), "X");
                        MessageBox.Show(y.ToString(), "Y"); */

                        g1.DrawLine(pen, x, y, x + 1, y);
                    }
                    statusBox.Text = "Mandelbrot-Set ready - please select zoom area with pressed mouse.";
                }
            action = true;
        }
        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;
            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }
        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            //e.consume();
            if (action)
            {
                xs = e.X;
                ys = e.Y;
            }
        }
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            int z, w;
            //e.consume();
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2)) initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                mandelbrot();
                rectangle = false;
                Refresh();
            }
        }
        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            //e.consume();
            if (action)
            {
                xe = e.X;
                ye = e.Y;
                rectangle = true;
                Refresh();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //init();
           // start();
        }
    }
}
